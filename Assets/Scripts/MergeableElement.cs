﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class MergeableElement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	[SerializeField] private Image image;
	public GridCell currentCell;
	public int level;

	public void OnBeginDrag(PointerEventData eventData)
	{
	}

	public void OnDrag(PointerEventData eventData)
	{
		transform.position = eventData.pointerCurrentRaycast.worldPosition;
	}

	public void LevelUp()
	{
		level++;
		image.sprite = GridManager.Instance.elementAssetHolder.assets.Find(x => x.level == level).sprite;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		var pos = eventData.pointerCurrentRaycast.worldPosition;
		var cell = GridManager.Instance.WorldToGrid(pos);
		if (currentCell != null && cell.element == currentCell)
		{
			transform.DOComplete();
			transform.DOMove(currentCell.transform.position, .1f);
			// if (_lastTween != null && !_lastTween.IsCompleted) _lastTween.Dispose();
			// _lastTween = MoveTween(currentCell.transform.position, .1f);
			return;
		}
		if (cell.element == null) // empty
		{
			currentCell = cell;
			cell.element = this;
			transform.DOComplete();
			transform.DOMove(cell.transform.position, 0.1f);
			// if (_lastTween != null && !_lastTween.IsCompleted) _lastTween.Dispose();
			// _lastTween = MoveTween(cell.transform.position, .1f);
		}
		else
		{
			if (cell.element.level == level) //merge
			{
				cell.element.LevelUp();
				Instantiate(GridManager.Instance.particle, cell.transform.position, Quaternion.identity);
				Destroy(gameObject);
			}
			else // back
			{
				transform.DOComplete();
				transform.DOMove(currentCell.transform.position, .1f);
				// if (_lastTween != null && !_lastTween.IsCompleted) _lastTween.Dispose();
				// _lastTween = MoveTween(currentCell.transform.position, .1f);
			}
		}
	}

	private Task _lastTween;

	private async Task MoveTween(Vector3 targetPosition, float duration)
	{
		var t = 0f;
		var initialPosition = transform.position;
		while (t <= duration)
		{
			transform.position = Vector3.Lerp(initialPosition, targetPosition, t / duration);
			await Task.Yield();
			t += Time.deltaTime;
		}
	}
}
