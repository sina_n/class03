﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ElementAsset
{
	public int level;
	public Sprite sprite;
}

[CreateAssetMenu(fileName = "ElementAssetHolder", menuName = "ScriptableObjects/ElementAssetHolder", order = 0)]
public class ElementAssetHolder : ScriptableObject
{
	public List<ElementAsset> assets;
}