﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviourSingleton<GridManager>
{
    [SerializeField] private List<GridCell> cells;
    public ElementAssetHolder elementAssetHolder;
    public GameObject particle;

    public GridCell WorldToGrid(Vector3 position)
    {
        var min = 1000f;
        var nearestCell = cells[0]; 
        foreach (var cell in cells)
        {
            var distance = Vector3.Distance(position, cell.transform.position);
            if (distance < min)
            {
                min = distance;
                nearestCell = cell;
            }
        }

        return nearestCell;
    }
}
